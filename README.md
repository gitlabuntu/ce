Install GitLab on Ubuntu 12.04
====

    Versions: GitLab [6.7.2]
              GitLab-Shell [1.9.1]

Setup-Guide
====
I. Get the Git
----
ssh

    sudo apt-get update && sudo apt-get -y upgrade
    sudo apt-get install -y git
    git clone git@gitlab.com:gitlabuntu/ce.git -b 12.04
https

    sudo apt-get update && sudo apt-get -y upgrade
    sudo apt-get install -y git
    git clone https://gitlab.com/gitlabuntu/ce.git -b 12.04
II. Initialize Ubuntu
----

    cd ce
    bash init
III. Install & Setup GitLab
----

    bash install